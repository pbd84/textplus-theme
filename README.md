# Text+ Theme for MInfBA Services

The theme is based on Bootstrap, Bootstrap Icons and the Roboto fonts and adapts the Portal design: https://gitlab.gwdg.de/textplus/portal

Bootstrap and Bootstrap Icons are included as git submodules.

## Installation

For theme installation see the group wiki at https://gitlab.com/groups/minfba/resinfra/themes/-/wikis/Theme-installation

## Translation status

![weblate](http://weblate.winseda.de/widget/registry/textplus-theme/287x66-black.png "Übersetzungsstatus")